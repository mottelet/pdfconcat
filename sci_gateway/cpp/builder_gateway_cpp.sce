// This file is released under the 3-clause BSD license. See COPYING-BSD.

function builder_gw_cpp()

    tbx_build_gateway("pdfconcat", ..
    ["pdfconcat","sci_pdfconcat","cppsci"], ..
    ["sci_pdfconcat.cpp","pdfconcat_service.cpp"], ..
    get_absolute_file_path("builder_gateway_cpp.sce"));

endfunction

builder_gw_cpp();
clear builder_gw_cpp; // remove builder_gw_cpp on stack
