// Scilab ( https://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2024 - UTC - Stéphane MOTTELET
//
// This file is released under the 3-clause BSD license. See COPYING-BSD.
//
#include <clocale>
#include <cwchar>
#include <iostream>
#include <string>
#include <vector>

#include "string.hxx"
#include "function.hxx"

extern "C"
{
#include "Scierror.h"
#include "localization.h"
#include "sciprint.h"
}

int pdfconcat(int argc, char const* const*argv, bool bVerb);

/* ==================================================================== */
types::Function::ReturnValue sci_pdfconcat(types::typed_list &in, int _iRetCount, types::typed_list &out)
{
    types::Function::ReturnValue ret = types::Function::OK;
    bool bVerb = false;
   
    if (in.size() < 2  || in.size() > 3)
    {
        Scierror(77, _("%s: Wrong number of input argument(s): %d to %d expected.\n"), "pdfconcat", 2,3);
        return types::Function::Error;
    }
    if (_iRetCount > 0)
    {
        Scierror(78, _("%s: Wrong number of output argument(s): %d expected."), "pdfconcat", 0);
        return types::Function::Error;
    }
    if (in[0]->isString() == false || in[0]->getAs<types::String>()->getSize() != 1)
    {
        Scierror(999, _("%s: Wrong type or size for input argument #%d: scalar string expected.\n"), "pdfconcat", 1);
        return types::Function::Error;
    }
    if (in[1]->isString() == false)
    {
        Scierror(999, _("%s: Wrong type for input argument #%d: string array expected.\n"), "pdfconcat");
        return types::Function::Error;
    }
    if (in.size() == 3)
    {
        if (in[2]->isBool() == false || in[2]->getAs<types::Bool>()->getSize() != 1)
        {
            Scierror(999, _("%s: Wrong type for input argument #%d: a scalar boolean expected.\n"), "pdfconcat",3);
            return types::Function::Error;
        }
        bVerb = in[2]->getAs<types::Bool>()->get(0);        
    }

    int iNbFiles = in[1]->getAs<types::String>()->getSize();

    char *args[2] = {"pdfconcat", "-o"};
    char **argv = (char **)malloc(sizeof(char*)*(iNbFiles+4));
    argv[0] = args[0];
    argv[1] = args[1];

    for (int i=0; i<iNbFiles+1; i++)
    {
        const wchar_t* wstr;
        if (i==0)
        {
            wstr=in[0]->getAs<types::String>()->get(0);            
        }
        else
        {
            wstr=in[1]->getAs<types::String>()->get(i-1);
        }
        std::mbstate_t state = std::mbstate_t();
        std::size_t len = 1 + std::wcsrtombs(nullptr, &wstr, 0, &state);
        argv[i+2] = (char *)malloc(sizeof(char)*len);
        std::wcsrtombs(argv[i+2], &wstr, len, &state);
    }
    argv[iNbFiles+3] = NULL;

    try
    {
        pdfconcat(iNbFiles+3,argv,bVerb);
    }
    catch (ast::InternalError& ie)
    {
        ret = types::Function::Error;
    }
    
    for (int i=0; i<iNbFiles; i++)
    {
        free(argv[i+2]);
    }
    free(argv);

    return ret;
}

