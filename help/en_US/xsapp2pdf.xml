<?xml version="1.0" encoding="UTF-8"?>
<!--
 Scilab ( https://www.scilab.org/ ) - This file is part of Scilab
 Copyright (C) 2024 - UTC - Stéphane MOTTELET
 This file is released under the 3-clause BSD license. See COPYING-BSD.
 -->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:ns5="http://www.w3.org/1999/xhtml" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" xmlns:scilab="http://www.scilab.org" xml:id="xsapp2pdf" xml:lang="en">
    <refnamediv>
        <refname>xsapp2pdf</refname>
        <refpurpose>export and append graphics to a multipage PDF</refpurpose>
    </refnamediv>
    <refsynopsisdiv>
        <title>Syntax</title>
        <synopsis>
            xsapp2pdf(win_num, file_name [,orientation])
            xsapp2pdf(hdl, file_name [,orientation])
        </synopsis>
    </refsynopsisdiv>
    <refsection>
        <title>Arguments</title>
        <variablelist>
            <varlistentry>
                <term>win_num</term>
                <listitem>
                    <para>an integer, ID of the Figure to export.</para>
                </listitem>
            </varlistentry>
            <varlistentry>
                <term>hdl</term>
                <listitem>
                    <para>handle of the Figure or Frame to export.</para>
                </listitem>
            </varlistentry>
            <varlistentry>
                <term>file_name</term>
                <listitem>
                    <para>a string, name of the exported file.</para>
                    <para>If the extension is not provided, it is going to be automatically added.</para>
                </listitem>
            </varlistentry>
            <varlistentry>
                <term>orientation</term>
                <listitem>
                    <para>
                        an optional string, with possible values
                        <literal>'portrait'</literal> or
                        <literal>'landscape'</literal>. The
                        default value is <literal>'portrait'</literal>.
                    </para>
                </listitem>
            </varlistentry>
        </variablelist>
    </refsection>
    <refsection>
        <title>Description</title>
        <para>
            <function>xsapp2pdf</function> appends the display of a Figure or a Frame to a PDF file. If the file does not exist, it is created.
        </para>
    </refsection>
    <refsection>
        <title>Examples</title>
        <programlisting role="example"><![CDATA[
            t = linspace(0,2*%pi,256);
            scf(0)
            for i=1:10
                clf
                plot(t,sin(i*t))
                xsapp2pdf(0,"file.pdf")
            end
 ]]></programlisting>
    </refsection>
    <refsection role="see also">
        <title>See also</title>
        <simplelist type="inline">
            <member>
                <link linkend="xs2pdf">xs2pdf</link>
            </member>
            <member>
                <link linkend="pdfconcat">pdfconcat</link>
            </member>
        </simplelist>
    </refsection>
</refentry>
