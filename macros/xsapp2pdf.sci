// Scilab ( https://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2024 - UTC - Stéphane MOTTELET
//
// This file is released under the 3-clause BSD license. See COPYING-BSD.
//
function xsapp2pdf(win_or_hdl, filename, orientation)
    rhs = argn(2);
    if rhs < 2 ||  rhs > 3 
        error(sprintf(_("%s: Wrong number of input argument(s): %d to %d expected.\n"),"xsapp2pdf",2,3))
    end
    if ~exists("orientation","local")
        orientation = "portrait";
    end
    
    if typeof(filename) ~= "string" || size(filename,"*") > 1
        error(sprintf(_("%s: Wrong size for input argument #%d: A single string expected.\n"),"xsapp2pdf",2));
    end

    if isfile(filename)
        [_, fname] = fileparts(filename);
        tmp = fullfile(TMPDIR,fname+".tmp");
        outfile = fullfile(TMPDIR,fname+".out");
        xs2pdf(win_or_hdl, tmp, orientation);
        pdfconcat(outfile,[filename,tmp]);
        deletefile(tmp);
        movefile(outfile,filename);
    else
        xs2pdf(win_or_hdl, filename, orientation);
    end
endfunction

